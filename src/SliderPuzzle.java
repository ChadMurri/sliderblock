import java.util.ArrayList;

class SliderPuzzle{
    //The Class Data for the size, last move taken, the coordinates of the blank space, and the board (a two dimensional array of ints).
    private int size = 3;
    private String lastMove;
    private int blankRow;
    private int blankCol;
    private int[][] board = new int[size][size];
    //Keeps track of moves so a solution can be presented when one is found
    private String moveList = "";
    //default constructor
    SliderPuzzle(){
        initialize();
        blankRow = size - 1;
        blankCol = size - 1;
        lastMove = "NA";
    }
    //clone constructor
    SliderPuzzle(SliderPuzzle original){
        for(int i = 0; i < original.getBoard().length; i ++){
            board[i] = original.getBoard()[i].clone();
        }
        lastMove = original.getLastMove();
        blankRow = original.getBlankRow();
        blankCol = original.getBlankCol();
        size = getSize();
        moveList = original.getMoveList();
    }
    //created a prejumbled puzzle
    SliderPuzzle(int timesJumbled){
        initialize();
        blankRow = size - 1;
        blankCol = size - 1;
        lastMove = "NA";
        jumble(timesJumbled);
    }
    //internal excessor methods used for cloning the class
    private int[][] getBoard(){
        return board;
    }
    private int getBlankRow(){
        return blankRow;
    }
    private int getBlankCol(){
        return blankCol;
    }
    private String getLastMove(){
        return lastMove;
    }
    private int getSize(){
        return size;
    }
    //gets the move list so it can be printed
    String getMoveList(){
        return moveList;
    }
    //adds moves to the move list
    void addMove(String direction){
        moveList += direction;
    }
    //resets the last move so that solving the puzzle doesn't immediately trip it
    void resetLastMove(){
        lastMove = "NA";
    }
    //Checks if a board is Solved by iterating through the 2d array
    boolean isSolved(){
        int count = 1;
        for(int i = 0; i < size; i ++){
            for(int n = 0; n < size; n ++){
                if(count == size * size){
                    return true;
                }
                if(board[i][n] != count){
                    return false;
                }
                count ++;
            }
        }
        return true;
    }
    //Checks if a chosen move is valid based on the last move taken and the the boundaries of the board.
    boolean isMoveValid(String direction){
        if(direction.compareTo("D") == 0 && lastMove.compareTo("U") != 0 && blankRow != 0){
            return true;
        }
        if(direction.compareTo("L") == 0 && lastMove.compareTo("R") != 0 && blankCol != size - 1){
            return true;
        }
        if(direction.compareTo("U") == 0 && lastMove.compareTo("D") != 0 && blankRow != size - 1){
            return true;
        }
        if(direction.compareTo("R") == 0 && lastMove.compareTo("L") != 0 && blankCol != 0){
            return true;
        }
        return false;
    }
    //moves a number into the blank. First the move is checked out to see if it's valid, and if it is the move executes
    //by replacing the blank with the slid tile and then setting the tiles previous spot to blank
    void move(String directions){
        String direction;
        for(int i = 0; i < directions.length(); i++) {
            direction = "" + directions.charAt(i);
            if(!isMoveValid(direction)){
                return;
            }
            if (direction.compareTo("D") == 0) {
                board[blankRow][blankCol] = board[blankRow - 1][blankCol];
                board[blankRow - 1][blankCol] = 0;
                blankRow--;
                lastMove = "D";
            }
            if (direction.compareTo("L") == 0) {
                board[blankRow][blankCol] = board[blankRow][blankCol + 1];
                board[blankRow][blankCol + 1] = 0;
                blankCol++;
                lastMove = "L";
            }
            if (direction.compareTo("U") == 0) {
                board[blankRow][blankCol] = board[blankRow + 1][blankCol];
                board[blankRow + 1][blankCol] = 0;
                blankRow++;
                lastMove = "U";
            }
            if (direction.compareTo("R") == 0) {
                board[blankRow][blankCol] = board[blankRow][blankCol - 1];
                board[blankRow][blankCol - 1] = 0;
                blankCol--;
                lastMove = "R";
            }
        }
    }
    //fills the board in in order
    private void initialize(){
        int count = 1;
        for(int i = 0; i < size; i++){
            for(int n = 0; n < size; n++){
                if(count == size * size){
                    board[i][n] = 0;
                }
                else {
                    board[i][n] = count;
                }
                count++;
            }
        }
    }
    //outputs the current state of the board
    void printBoard(){
        for(int i = 0; i < size; i++){
            for(int n = 0; n < size; n++){
                System.out.printf("%-3d", board[i][n]);
            }
            System.out.println();
        }
    }
    //takes an int and then randomly scrambles the puzzle using valid moves (so it always moves the inputted amount)
    void jumble(int timesJumbled){
        int randInput;
        int count = 0;
        String[] moves = {"U", "D", "L", "R"};
        while(count < timesJumbled){
            randInput = (int)(4 * java.lang.Math.random());
            if(isMoveValid(moves[randInput])){
                move(moves[randInput]);
                count++;
            }
        }
    }
}
